/*jshint sub:true*/

 "use strict";

var _und = require('underscore');
var Engine = require('tingodb')();
var Q = require('q');

var db,documents,indexCollection,users;

function initialize(){
	documents = db.collection('data');
	indexCollection = db.collection('indexedWords');
	users = db.collection('users');
}
function insertToData(doc,need_index,callback){
	if (!doc) return 'Error2';
	documents.insert(doc,function(err,docs){
		if (err) {
			console.log(err);
			//throw 'Error';
		}
		if(need_index) {
			indexDocument(docs,callback);
		}else{
		callback();
		}
	});
	return 'Success';
}
function update(doc,callback){
	if(!doc) return 'Error2';
	delete doc["$$hashKey"];
	documents.save(doc,function(err,results){
		if (err) {throw err;}
		indexDocument([doc],callback);
	});
	return 'Success';
}
function indexDocument(docs,callback){
	//console.log("starting");
	_und.each(docs,function(doc){
		var id = doc._id;
		//console.log(doc);
		delete doc['_id'];
		var wordlist = [];
		_und.each(doc ,function(value,key){
		//console.log(value)
		if(key.indexOf("__") === 0) {
			return;
		}
		if(key === 'type') {
			return;
		}
		if(_und.isDate(value)){
			var temp = value.toString(); 
			value = temp;
		}
		_und.each(value.match(/\S+/g),function(key){ wordlist.push(key); } );
		});
		//console.log("All words:",wordlist);
		//console.log("Uniq words",_und.uniq(wordlist))
		var count =0;
		_und.each(_und.uniq(wordlist),function(value){
			indexCollection.update(
				{'word': value},
				{ $push: {'key': id}},
				{upsert:true},
				function(err,doc){
					//if(err) console.log(err);
					count++;	
					if(count >= _und.uniq(wordlist).length){
						callback();
					}
				});
			});
		
	});
}
function get(doc,callback){
	//console.log(doc);
	if(doc.useIndex === undefined){
		documents.find(doc).toArray(function(err,docs){
			callback(null,docs);
		});
	}else{
		indexCollection.findOne({'word': doc.useIndex}, function(err,row){
			var results = [];
			//console.log('err=',row);
            //console.log('row=',row);
            if(_und.isUndefined(row)){
                callback(null,results);   
                return;
            }
            _und.each(row.key,function(id){
				//console.log(id,row.key.length,results);
				var term = {"_id":id};
				documents.findOne(term,function(err,ret){
					results.push(ret);
					if(results.length == row.key.length){
						callback(null,results);
					}
				});
			});
		});
	}
}
function getIndex(callback){
	indexCollection.find({}).toArray(function(err,results){
		//console.log('err=',err);
		//console.log('results=',results);
		callback(results);
	});
}
function dbDump(writer){
	get({},function(err,docs){
		_und.each(docs,function(doc){
			writer.write(doc);
		});
	});
}
function dbClear(){
	
}
var DAL = function(config){
	if(!_und.has(config,'path'))  throw "You must supply a path";
	var dbpath = config.path;
	db = new Engine.Db(dbpath, {});
	initialize();
	
	return {
		'data' : documents,
		'index' : indexCollection,
		'users' : users,
		'insert' : insertToData,
		'get'	: get,
		'getIndex' : getIndex,
		'close' : db.close,
		'update' : update
	};
};


module.exports = DAL;

