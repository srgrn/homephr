var  expect = require('expect.js');
var fs = require('fs');
var DAL = require('../DAL.js')({'path': './test/'});
fs.openSync('./test/data','w');
fs.openSync('./test/indexedWords','w');

describe('Data Access Layer', function(){
	describe('insert',function(){
		//console.log(DAL);
		it('can save a document',function(done){
			doc = { 'test':'a','key':123};
			expect('Success').to.be.equal(DAL.insert(doc,false,
				function(){
					DAL.data.findOne(doc,function(err,item){
						expect(doc.key).to.be.equal(item.key);
						done();
					});
				}));
		});
		it('can save a document with indexing',function(done){
			doc = { 'name':'billi ray kanck','address':'Ha Lamed He 125 Givattime'};
			expect('Success').to.be.equal(DAL.insert(doc,true,
				function(){
					DAL.data.findOne(doc,function(err,item){
						expect(doc.key).to.be.equal(item.key);
						done();
					});
				}));
		});
	});

	describe('find',function(){
		it('can get the single document',function(done){
			doc = { 'name':'jack bauer','address':'SF somewhere'};
			expect('Success').to.be.equal(DAL.insert(doc,true,
			function(){
					DAL.get(doc,function(err,item){
						expect(doc.key).to.be.equal(item.key);
						done();
					});
				}));
		});
		it('can get documents of index',function(done){
			doc = { 'name':'דני סנדרסון','address':'Luna City'};
			DAL.insert(doc,true,function(err,doc){
				DAL.get({'useIndex': 'דני'},function(err,items){
						expect(items.length).to.be.equal(1);
						//console.log(items);
						done();
				});	
			});
		});
		it('can get nothing',function(done){
			DAL.get({'useIndex': 'hhh'},function(err,items){
					expect(items.length).to.be.equal(0);
					//console.log(items);
					done();
			});	
		});
		it('can get the entire index',function(done){
			DAL.getIndex(function(items){
				//console.log(items);
				expect(items.length).to.be.equal(16);
				done();
			});
		});
	});
	describe('update',function(){
		it('can update a single document',function(done){
			doc = { 'name':'some mistake','address':'New York'};
			expect('Success').to.be.equal(DAL.insert(doc,true,
			function(){
					DAL.get(doc,function(err,items){
						items[0].name = "NoMistake";
						DAL.update(items[0],function(err,result){
							done();	
						});
					});
				}));
		});
	});
});