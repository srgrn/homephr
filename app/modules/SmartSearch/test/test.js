var  expect = require('expect.js'),
       SmartSearch = require('../SmartSearch.js');
//console.log('Testing SmartSearch Module');
describe('SmartSearch', function(){
    //console.log('damerauLevenshteinDistance()');
    describe('damerauLevenshteinDistance()', function(){
        it('Should return 0 when both strings are empty', function(){
            expect(0).to.be.eql(SmartSearch.damerauLevenshteinDistance());
        });
        it('Should return target length when source string is empty and vice versa', function(){
            expect(5).to.be.eql(SmartSearch.damerauLevenshteinDistance("","hello"));
            expect(5).to.be.eql(SmartSearch.damerauLevenshteinDistance("hello",""));
        });
        it('return distance 1 for single operations', function(){
            expect(1).to.be.eql(SmartSearch.damerauLevenshteinDistance("aba","aab"));
            expect(1).to.be.eql(SmartSearch.damerauLevenshteinDistance("aab","aac"));
            expect(1).to.be.eql(SmartSearch.damerauLevenshteinDistance("aab","aaba"));
            expect(1).to.be.eql(SmartSearch.damerauLevenshteinDistance("aab","aa"));
        });
        it('return correct distance for complex words - agraba -> algebra = 4', function(){
              expect(4).to.be.eql(SmartSearch.damerauLevenshteinDistance('agraba','algebra'));
        });
    });
    //console.log('levensteinDistance()');
    describe('levensteinDistance()', function(){
        it('Should return 0 when both strings are empty', function(){
           expect(0).to.be.eql(SmartSearch.levensteinDistance());
        });
        it('Should return target length when source string is empty and vice versa', function(){
          expect(5).to.be.eql(SmartSearch.levensteinDistance("","hello"));
          expect(5).to.be.eql(SmartSearch.levensteinDistance("hello",""));
        });
        it('return distance 1 for single operations', function(){
            expect(1).to.be.eql(SmartSearch.levensteinDistance("aab","aac"));
            expect(1).to.be.eql(SmartSearch.levensteinDistance("aab","aaba"));
            expect(1).to.be.eql(SmartSearch.levensteinDistance("aab","aa"));
        });
    });
      //console.log('buildCollection()');
    describe('buildCollection()', function(){
        var arr = [
            {'word':'book','list':[1,2,3]},
            {'word':'boker','list':[1,2,3]},
            {'word':'cook','list':[1,2,3]}
            ];
        var orig = 'book';
        it('return new array of objects with distances', function(){
            expect(0).to.be.eql(SmartSearch.buildCollection(orig,arr)[0].distance);
        });
    });
    describe('search()',function(){
        var arr = [
            {'word':'doctor','list':[1,2,3]},
            {'word':'prescribe','list':[1,2,3]},
            {'word':'prescription','list':[1,2,3]},
            {'word':'umglout','list':[1,2,3]},
            {'word':'seen','list':[1,2,3]}
            ];
        var orig = 'prescription';
        it('returns all valid results',function(){
            result = SmartSearch.search(orig,arr);
            //console.log(result);
            expect(2).to.be.eql(result.length);
        });
    });
});