'use strict';
var SmartSearch = function(obj){
    var ret = obj || {};
    var _und = require('underscore');
    
    function isStringNotEmpty(str){
        return (_und.isString(str) && str.length > 0);
    }   
    ret.contains = function(source,arr){
        var result = _und.filter(arr,function(str){
            return str.indexOf(source) != -1;
        });
        return result; /* result is an array of values returned of words containing the source word*/
    };
    
    ret.levensteinDistance = function (source,target){
        if(!isStringNotEmpty(target) && !isStringNotEmpty(source)) return 0;
        if(!isStringNotEmpty(source)) return target.length;
        if(!isStringNotEmpty(target)) return source.length;
        
        var cost = (source[source.length-1] == target[target.length-1]) ? 0 : 1; /* if true cost = 0 otherwise 1 */
        return _und.min([
            this.levensteinDistance(source.slice(0,-1),target) + 1,
            this.levensteinDistance(source,target.slice(0,-1)) + 1,
            this.levensteinDistance(source.slice(0,-1),target.slice(0,-1)) + cost
        ]);
    };
    /* this function is Dameru*/
    ret.damerauLevenshteinDistance = function(source,target){
        var i,j,max,DB;
        var chars = {};
        var score = [];
        if(!isStringNotEmpty(target) && !isStringNotEmpty(source)) return 0;
        if(!isStringNotEmpty(source)) return target.length;
        if(!isStringNotEmpty(target)) return source.length;
        
        max = source.length + target.length;
        score[0] = [];
        score[0][0] = max;
        for (i = 0; i <= source.length; i++) { 
            score[i+1] = [];
            score[i + 1][1] = i; score[i + 1][0] = max; 
        }
        for (j = 0; j <= target.length; j++) { 
            score[1][j + 1] = j; score[0][j + 1] = max; 
        }
        
        _und.each(source + target,function(ch){chars[ch] = 0;});
        for (i = 1; i <= source.length; i++){
            DB = 0;
            for (j = 1; j <= target.length; j++){
                var i1 = chars[target[j - 1]];
                var j1 = DB;
     
                if (source[i - 1] === target[j - 1]) {
                    score[i + 1][j + 1] = score[i][j];
                    DB = j;
                } else {
                    score[i + 1][j + 1] = _und.min([score[i][j], score[i + 1][j], score[i][j + 1]]) + 1;
                }
                score[i + 1][j + 1] = _und.min([score[i + 1][j + 1], score[i1][j1] + (i - i1 - 1) + 1 + (j - j1 - 1)]);
            }
 
            chars[source[i - 1]] = i;
        }
        return score[source.length + 1][target.length + 1];

    };
    ret.buildCollection = function(term,db){
        return _und.map(db, 
            function(value){
                var result = value;
                result.distance = ret.damerauLevenshteinDistance(term,value.word);
                return result;
            });
    };
    ret.search = function(term,db,requiredSensetivity,caseInSensetive){
        var sensetivity = 0.5;
        if(requiredSensetivity) {
            sensetivity = requiredSensetivity/10;
        }
        var cis = caseInSensetive;
        console.log(caseInSensetive);
        var single = false;
        var filteredResults = _und.filter(
                ret.buildCollection(term,db), 
                function(value){
                    var temp_term = term;
                    var temp_word = value.word;
                    if(caseInSensetive) {
                        temp_term = term.toLowerCase();
                        temp_word = value.word.toLowerCase();
                    }
                    var distanceShorterThenTerm = value.distance<=temp_term.length;
                    var distanceShorterThenWord = value.distance < (temp_word.length * sensetivity);
                    var termPartOfWord = (temp_word.indexOf(temp_term) > -1);
                    if(termPartOfWord) { single = true;} 
                    var ret = ( (distanceShorterThenTerm && distanceShorterThenWord) || termPartOfWord);
                    //console.log(ret,value.distance,(value.word.length * sensetivity),term.length,value.word,distanceShorterThenTerm,distanceShorterThenWord,termNotPartOfWord);
                    return ret;
                }
            );
        if (single) {
            filteredResults = _und.filter(filteredResults, function(value){ 
                if(caseInSensetive) { return value.word.toLowerCase().indexOf(term.toLowerCase()) > -1; }
                return value.word.indexOf(term) > -1; 
            });
        }
        return _und.sortBy(filteredResults, function(value){ return value.distance;});
    };
    return ret;

};

module.exports = new SmartSearch();
