var  expect = require('expect.js');
describe('Authenticator', function(){
	var usersDB = [];
	var Auth = require('../Authenticator.js');
	describe('Create user',function(){
		it('create a new password',function(){
			var password = "password";
			var result = Auth.addUser('test',password);
			//console.log(Auth.user);
			expect(result).to.be.ok();
			expect(Auth.user).to.be.ok();
			expect('test').to.be.equal(Auth.getUser().username);
			expect(password).not.to.be(Auth.getUser().password);
			usersDB.push(Auth.getUser());
		});
	});
	describe('login/logout',function(){
		it('should allow login when user is empty',function(){
			if(Auth.user){
				Auth.user = undefined;
			}
			var result = Auth.login('test','password',usersDB);
			expect(result).to.be.ok();
			expect(Auth.user).to.be.ok();
			expect('test').to.be.equal(Auth.getUser().username);
		});
		it('logout will clear the user object',function(){
			expect(Auth.user).to.be.ok();
			var result = Auth.logout();
			expect(Auth.user).not.be.ok();
		});
	});
	
});