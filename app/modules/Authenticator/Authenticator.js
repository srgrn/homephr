/*jshint sub:true*/
"use strict";
function Auth() { 
	var _ = require('underscore');
	var crypto = require('crypto-js');
	//var userObj = undefined;
	function cryptPassword(password) {
		return crypto.SHA512(password).toString();
	}
	function comparePassword(password, userPassword) {
		var enc = crypto.SHA512(password).toString();
		return enc === userPassword;
	}
	function login(username,password,usersDB){
		var userValue = _.find(usersDB, function(value){ return value.username == username;});
		if(_.isUndefined(userValue)) { return false;}
		var isMatch = comparePassword(password,userValue.password);
		if(isMatch){
			ret.user = {};
			ret.user.username = username;
			ret.user.fullname = userValue.fullname;
			ret.user.allowedUsers = userValue.allowd;
		}
		return checkUser();
	}
	function logout(){
		ret.user = null;
	}
	function checkUser(){
		if(_.isNull(ret.user) || _.isUndefined(ret.user)) {
			return false;
		}
		return true;
	}
	function createUser(username,password){
		ret.user = {};
		ret.user.username = username;
		ret.user.password = cryptPassword(password);
		ret.user.fullname = null;
		ret.user.allowedUsers = [];
		return checkUser();
	}
	function getUser(){
		return ret.user;
	}
	var ret = {
		'isLoggedIn' : checkUser,
		'getUser' : getUser,
		'login' : login,
		'logout': logout,
		'addUser': createUser,
		'user': undefined
	};
	return ret;
}


module.exports = new Auth();