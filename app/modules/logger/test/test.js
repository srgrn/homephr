var  expect = require('expect.js');
var output = "";
var origlog = console.log;
var replacelog = function(item){ output = item;};
describe('Logger console', function(){
	console.log = replacelog;
	var log = require('../logger.js')("console");
	it("can send messages to console", function(){
			log.Notice("Test notice");
			expect(new Date().toLocaleString() + "Notice:Test notice").to.be.eql(output);
			log.Warn("Test notice");
			expect(new Date().toLocaleString() + ": WARN:Test notice").to.be.eql(output);
			log.Info("Test notice");
			expect(new Date().toLocaleString() + ": INFO:Test notice").to.be.eql(output);
			log.Error("Test notice");
			expect(new Date().toLocaleString() + ": ERROR:Test notice").to.be.eql(output);
	});

	console.log = origlog;
});
describe('Logger file', function(){
	var log = require('../logger.js')("./test.log");
	
	var fs = require('fs');
	fs.unlinkSync('./test.log');
	describe('Notice',function(){
		it("can send messages to file", function(){
			log.Notice("Test notice");
			log.Warn("Test notice");
			log.Info("Test notice");
			log.Error("Test notice");
			output = fs.readFileSync("./test.log",{encoding:'UTF8'});
			expected = new Date().toLocaleString() + "Notice:Test notice\n" + 
						new Date().toLocaleString() + ": WARN:Test notice\n" +
						new Date().toLocaleString() + ": INFO:Test notice\n" +
						new Date().toLocaleString() + ": ERROR:Test notice\n";
			expect(expected).to.be.eql(output);
		});
	});
});