"use strict";

var Logger = function(logLocation){
    var write = null;
    var _format = "defualt";
    var fs = require('fs');
    if (logLocation === "console"){
        write = console.log;
    }else {
        write = (function(logLocation) {
            var b = function(data) {
                return fs.appendFileSync(logLocation,data + "\n");
            };
            return b;
        })(logLocation);
    }


    function message(format,msg){
        var string = format + msg;
        write(string);
    }
    return {
        'Info' : message.bind(this,new Date().toLocaleString()+": INFO:"),
        'Error' : message.bind(this,new Date().toLocaleString()+": ERROR:"),
        'Debug' : message.bind(this,new Date().toLocaleString()+": DEBUG:"),
        'Warn'  : message.bind(this,new Date().toLocaleString()+": WARN:"),
        'Notice': message.bind(this,new Date().toLocaleString()+ "Notice:"),
        'format' : _format
    };
};

module.exports = Logger;
