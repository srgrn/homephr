var  expect = require('expect.js');
var fs = require('fs');
var exporter = require('../exporter.js');

var jsonPath = './exporttest.json';
var sfrPath = './exporttest.sfr';
var singleDoc = {
	'a': 'some value',
	'date': 'Some Date as string',
	'object': { 'a': 'b'}
};
describe('Exp-Imp Module', function(){
	if(fs.existsSync(jsonPath)) {	fs.unlinkSync(jsonPath); }
	if(fs.existsSync(sfrPath)) {	fs.unlinkSync(sfrPath); }
	describe('Export',function(){
		it('can export a single document to json',function(done){
			exporter.export(singleDoc,jsonPath,'json');
			fs.exists(jsonPath, function (exists) {
				if(exists) {
					done();	
				}
			});
			
		});
		it('can export a all documents to json',function(done){
		done();
		});
		it('can export a single document to stupidFormat(tm)',function(done){
			exporter.export(singleDoc,sfrPath,'sfr');
			fs.exists(sfrPath, function (exists) {
				if(exists) {
					done();	
				}
			});
		});
		it('can export a all documents to stupidFormat(tm)',function(done){
		done();
		});
	});

});