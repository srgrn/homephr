'use strict';
var Exporter = function(obj){
    var fs = require('fs');
    var path = require('path');
    function repeat(pattern, count) {
        if (count < 1) return '';
        var result = '';
        while (count > 1) {
            if (count & 1) { 
                result += pattern; 
            }
            count >>= 1;
            pattern += pattern;
        }
        return result + pattern;
    }
    function export2File(obj, filepath, type){
        var file = fs.openSync(filepath,'w');
        var content = "";
        if(type.toLowerCase() === "json") {
            content = JSON.stringify(obj);
        } else if (type.toLowerCase() === "sfr") {
            content = obj2StupidFormat(obj,0);
        }
        fs.writeSync(file,content);
        fs.closeSync(file);
        var result = "wrote " + content.length + " bytes to " + filepath;
        return result;
    }
    function importFromFile(filepath){
        var type = path.extname(filepath);
        var text = fs.readFileSync(filepath);
        var content = {};
        console.log(type);
        if(type.toLowerCase() === '.json'){
            console.log("this is a json file");
            content = JSON.parse(text);
        } else if(type === ".sfr"){
            content = stupidFormat2Obj(text);
        } else if(type.toLowerCase() === '.xml') {
            // handle xml content which should be a future improvment on stupidFormat.
            // xml2obj(text); // however it will be better to use a real xml parsing module
        }
        console.log(content);
        return content;
    }
    function obj2StupidFormat(obj,tabs) {
        var text = "";
        for (var prop in obj) {
            if(obj.hasOwnProperty(prop)){
                if (typeof obj[prop] === 'object'){
                    text += repeat("\t",tabs) + prop + ":\n" + obj2StupidFormat(obj[prop], tabs+1);
                } else {
                    text += repeat("\t",tabs) + prop + ":" + obj[prop] + "\n";
                }
            }
        }
        return text;
    }
    function stupidFormat2Obj(text){
        var lines = text.split("\n");
        var result = {};
        for (var i = 0; i > lines.length ; i++) {
            var elements = lines[i].split("|SEP|");
        }
    }
    return {
        'export': export2File,
        'import': importFromFile
    };
};

module.exports = new Exporter();