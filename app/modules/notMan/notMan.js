"use strict";
var Manager = function(){
	require('datejs');
	var _ = require('underscore');
	var i =0;
	var regular = 7;
	var important = 4;
	var critical = 1;
	var ret = {};
	var levels = [undefined,'critical','important','regular'];
	var types = {'critical': 'warning','important':'info','regular':'success'};
	function parseDate(input) {
		var parts = input.split('/');
		return new Date(parts[2], parts[1]-1, parts[0]); // Note: months are 0-based
	}
	function checkDate(date)
	{
		var targetDate = parseDate(date);
		var nowDate = Date.today();
		var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
		var diffDays = Math.round((targetDate.getTime() - nowDate.getTime())/(oneDay));
		return diffDays;
	}

	function searchData(level,settings) {
		if(settings) {
			regular = settings.regular;
			important = settings.important;
			critical = settings.critical;
		}
		if (levels.indexOf(level) < 0 ) { throw "This cannot be";}
		var found = {};
		// this is stupid coding however i am no longer caring. and i don't htink anyone will read this code.
		found[levels[1]] = [];
		found[levels[2]] = [];
		found[levels[3]] = [];
		var valid_prescriptions = [];
		var vp_names = [];
		for (i = ret.data.length - 1; i >= 0; i--) {
			var item = ret.data[i];
			if (!_.isUndefined(item.type)){
				var daysLeft;
				var endDate;
				var message = "";
				switch (item.type){
					case "prescription":
						if ( _.isUndefined(item.startDate) || _.isUndefined(item.dose) || _.isUndefined(item.amount)) { break;}
						var measure = 0;
						switch (true) {
							case /daily/.test(item.dose):
								if (/twice/.test(item.dose)) {
									measure = Math.round(item.amount / 2);
								} else if (/once/.test(item.dose)) {
									measure = Math.round(item.amount / 1);
								} else {
									measure = Math.round(item.amount / parseInt(item.dose));
								}
								break;
							case /hour/.test(item.dose):
								measure = Math.round(item.amount / parseInt(item.dose) / 24);
								break;
							case /\/d/.test(item.dose):
								measure = Math.round(item.amount / parseInt(item.dose));
								break;
							case /\/h/.test(item.dose):
								measure = Math.round(item.amount / parseInt(item.dose) / 24);
								break;
							default:
								break;
						}
						endDate = Date.parse(item.startDate,"dd/MM/yyyy").addDays(measure).toString("dd/MM/yyyy");
						daysLeft = checkDate(endDate);
						if(daysLeft > 0) { valid_prescriptions.push(item); vp_names.push(item.name);}
						message = "Your prescription of " + item.name + " is about to end in " + daysLeft + " days.";
						if( daysLeft > critical && daysLeft < important) {
							found[levels[2]].push(createNotification(levels[2],message,item._id));
						}else if (daysLeft <= critical && daysLeft >= 0) {
							found[levels[1]].push(createNotification(levels[1],message,item._id));
						}else if( daysLeft > important && daysLeft <= regular) {
							found[levels[3]].push(createNotification(levels[3],message,item._id));
						}
						break;
					case "appointment":
						endDate = item.date.toString("dd/MM/yyyy");
						daysLeft = checkDate(endDate);
						message = "You have appointment on:" + item.date.toString("dd/MM/yyyy") + " at " + item.time.toString("HH:mm:ss") + " with doctor " + item.doctor + ".";
						if( daysLeft > critical && daysLeft < important) {
							found[levels[2]].push(createNotification(levels[2],message,item._id));
						}else if (daysLeft <= critical && daysLeft >= 0) {
							found[levels[1]].push(createNotification(levels[1],message,item._id));
						}else if( daysLeft >= important && daysLeft <= regular) {
							found[levels[3]].push(createNotification(levels[3],message,item._id));
						}
						break;	
					case "test":	
						endDate = item.date.toString("dd/MM/yyyy");
						daysLeft = checkDate(endDate);
						message = "You have test on:" + item.date.toString("dd/MM/yyyy") + " at " + item.time.toString("HH:mm:ss") + " with doctor " + item.doctor + ".";
						if( daysLeft > critical && daysLeft < important) {
							found[levels[2]].push(createNotification(levels[2],message,item._id));
						}else if (daysLeft <= critical && daysLeft >= 0) {
							found[levels[1]].push(createNotification(levels[1],message,item._id));
						}else if( daysLeft >= important && daysLeft <= regular) {
							found[levels[3]].push(createNotification(levels[3],message,item._id));
						}
						break;
					default:
						break;	
				}
			}
		}
		//console.log(vp_names);
		if(valid_prescriptions.length > 0) {
			for(var n = valid_prescriptions.length - 1; n >=0; n--){
				var interactions = _.pluck(valid_prescriptions[n].__InteractionsList,"name");
				var intersection = [];
				for (var c = interactions.length - 1; c >= 0; c--) {
					for (var i2 = vp_names.length - 1; i2 >= 0; i2--) {
						if (interactions[c].indexOf(vp_names[i2]) > -1) {
							intersection.push(vp_names[i2]);
						}
						
					}
					
				}
				//console.log(intersection);
				intersection = _.uniq(intersection);
				for (var i3 = intersection.length - 1; i3 >= 0; i3--) {
					var alert = "There is drug interaction between " + valid_prescriptions[n].name + " and " + intersection[i3];
					//var id = {'id': valid_prescriptions[n]._id.id + 1000000};
					found[levels[1]].push(createNotification(levels[1],alert));
					//console.log(alert,id);
				}

			}
		}
		if(_.isUndefined(level)) { return _.union(found[levels[3]],found[levels[2]],found[levels[1]]);}
		return found[level];
	}
	function createNotification(level,message,extraDetails){
		return {
			msg: message,
			type: types[level],
			canceled: "no"
		};
	}

	ret = {
		'data' : [],
		'getCritical' : searchData.bind(this,'critical'),
		'getImportant' : searchData.bind(this,'important'),
		'getRegular': searchData.bind(this,'regular'),
		'getAll': searchData.bind(this,undefined)
	};
	return ret;
};


module.exports = new Manager();