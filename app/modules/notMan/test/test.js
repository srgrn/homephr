var  expect = require('expect.js');
var fs = require('fs');
require('datejs');
var manager = require('../notMan.js');

describe('Notification Manger Module', function(){
	describe('can get specific types of notifications',function(){
		var oneWeekFromNow = Date.today().addDays(7).toString("dd/MM/yyyy");
		var tomorrow = Date.today().addDays(1).toString("dd/MM/yyyy");
		var threeDaysAgo = Date.today().addDays(-3).toString("dd/MM/yyyy");
		manager.data = [
			{date:tomorrow ,type:'appointment', time:"14:00"},
			{date: oneWeekFromNow,type:'appointment',time:"14:00"},
			{startDate: threeDaysAgo, type: 'prescription', name:'xanax',dose:'2/d',amount:10},
			{value:'this is a notification',type:'notification'}
		];
		it('critical only',function(){
			expect(manager.getCritical()).to.not.be.empty();
			//expect(manager.getRegular()).to.eql([]);
		});
		it('important only',function(){
			expect(manager.getImportant()).to.not.be.empty();
			//expect(manager.getRegular()).to.eql([]);
		});
		it('regular only',function(){
			expect(manager.getRegular()).to.not.be.empty();
			//expect(manager.getRegular()).to.eql([]);
		});
		it('All',function(){
			expect(manager.getAll()).to.not.be.empty();
			//expect(manager.getRegular()).to.eql([]);
		});
	});
	describe('expected errors',function(){
		it('no data should throw an error',function(){
			manager.data = undefined;
			expect(manager.getAll).to.throwError();
		});
	});
});