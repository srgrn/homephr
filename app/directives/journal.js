homephr.directive('journal', function() {
	return {
		restrict: 'E',
		templateUrl: "templates/journal.html",
        scope: {
			item: '=',
			condensed: '='
      }
	};
});