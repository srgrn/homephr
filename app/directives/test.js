homephr.directive('test', function() {
	return {
		restrict: 'E',
		templateUrl: "templates/test.html",
        scope: {
			item: '=',
			condensed: '='
      }
	};
});