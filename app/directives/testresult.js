homephr.directive('testresult', function() {
	return {
		restrict: 'E',
		templateUrl: "templates/testresult.html",
        scope: {
			item: '=',
			condensed: '='
      }
	};
});