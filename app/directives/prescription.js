/*jshint -W069 */
homephr.directive('prescription',  ["$compile", "$timeout",'$q','Scraper', function ($compile, $timeout,$q,Scraper) {
	return {
		restrict: 'E',
		templateUrl: "templates/prescription.html",
		scope: {
			item: '=',
			condensed: '='
			},

			link: function (scope, element, attrs, controller) {
				scope.allResults = [];
				scope.getResults = function(typedthings){
					Scraper.getPromiseResults(typedthings).then(function(data){
						var _ = require('underscore');
						scope.results = _.pluck(data,'name');
						scope.allResults = data;
					});
				};
				scope.results = [];
				scope.attachData = function(name) {
					// this is a foolish notion, i'm hoping to update the values to make it easier for me later.
					// but since it doesn't actually work it just do more work for the code.
					var _ = require('underscore');
					var extended = _.where(scope.allResults, {'name': name})[0];
					scope.item.substance_name = extended.substance_name;
					scope.item.__side_effects_link = extended.__side_effects;
					scope.item.__interactions_link = extended.__interactions_link;
					Scraper.getLink(extended).then(function(link){
						scope.item.link = link;
					});
					Scraper.getInteractionsList(extended).then(function(list){
						scope.item['__InteractionsList'] = list;
					});
				};
				scope.clicklink = function(link) {
					var gui = require('nw.gui');
					gui.Shell.openExternal(link);
				};
		}
	};
}]);