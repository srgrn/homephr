homephr.directive('user', function() {
	return {
		restrict: 'E',
		templateUrl: "templates/user.html",
        scope: {
			item: '=',
			condensed: '='
      }
	};
});