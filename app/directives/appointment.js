homephr.directive('appointment', function() {
	return {
		restrict: 'E',
		templateUrl: "templates/appointment.html",
        scope: {
			item: '=',
			condensed: '='
      }
	};
});