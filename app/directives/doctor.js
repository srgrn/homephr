homephr.directive('doctor', function() {
	return {
		restrict: 'E',
		templateUrl: "templates/doctor.html",
        scope: {
			item: '=',
			condensed: '='	
      }
	};
});