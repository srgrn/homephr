
var homephr = angular.module('HomePHR',['ngRoute','ui.bootstrap','DWand.nw-fileDialog','autocomplete']).config(
    function ($routeProvider,$httpProvider) {
    "use strict";
        //Whitelist file urls
        //$compileProvider.aHrefSanitizationWhitelist(/^\s*(file):/);

        //Routers
        $routeProvider
            .when('/', {path: 'home'})
            .otherwise({redirectTo: '/'});
         delete $httpProvider.defaults.headers.common['X-Requested-With'];
	});

