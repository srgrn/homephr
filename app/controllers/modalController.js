
homephr.controller('ModalAddCtrl', ['$scope','$modalInstance','$q','type','item', function($scope,$modalInstance,$q,type,item){
	"use strict";
	$scope.item = item;
	$scope.templateUrl = "templates/title-bar.html";
	$scope.type = type;

	if($scope.type === "journal"){
		var dNow = new Date();
		$scope.item.date = dNow.getMonth() + '/' + dNow.getDate() + '/' + dNow.getFullYear() + ' ' + dNow.getHours() + ':' + dNow.getMinutes();
	}
	$scope.ok = function () {
		console.log($scope.item);
		$modalInstance.close({type: $scope.type, item: $scope.item});
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};

	
}]);
homephr.controller('SettingsCtrl', ['$scope', '$modalInstance', function($scope,$modalInstance){
	"use strict";
	$scope.settings = window.settings;
	$scope.save = function () {
		// write settings to file
		var fs = require('fs');
		var path = require('path');
		var workDir = path.dirname( process.execPath );
		fs.open(workDir + '/settings.json', 'w', function(err,fd){
			fs.write(fd, JSON.stringify($scope.settings));
		});
		window.settings = $scope.settings;
		$modalInstance.close();
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');

	};
}]);
