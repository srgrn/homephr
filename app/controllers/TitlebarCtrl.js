homephr.controller('LoginModal', ['$scope','Logger','Auth','$modalInstance', function($scope,Logger,Auth,$modalInstance){
    $scope.ok = function (username,password) {
        console.log(username,password);
        if(Auth.login(username,password,Auth.usersDB)){ 
            Logger.Info("user "+username +" logged in Successfully ");
            $modalInstance.close();
        } else {
            $scope.message = "user or password incorrect";
            // too much magic in this code. it is stinking of magic.
            Logger.Error("user:" + username + " password:" + password + "  wrong credentials entered");
        }   
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);


homephr.controller('TitlebarCtrl', ['$scope','Window','Logger','DAL','SmartSearch','Auth','fileDialog','$modal', function($scope,Window,Logger,DAL,SmartSearch,Auth,fileDialog,$modal){
                "use strict";
    var _ = require('underscore');
    $scope.menuNames = ['App Menu','Add New'];
    var loggedOut = [
        [
            {text: "minimize",func:'minimize'},
            {text: "login",func:'login'},
            {text: "Create User",func:"addSome"},
            {text: "Quit",func:'close'},
            ],
            []
        ];  

    var loggedIN = [
        [
            {text: "minimize",func:'minimize'},
            //{text: "login",func:'login'},
            {text: "Settings",func:'openSettings'},
            {text: "logout",func:'logout'},
            {text: "Export",func:"exportData"},
            {text: "Import",func:"importData"},
            {text: "Create User",func:"addSome"},
            {text: "Quit",func:'close'},
        ],
        [
            {text: "Add Prescription",func:"addSome"},
            {text: "Add Doctor",func:"addSome"},
            {text: "Add Journal",func:"addSome"},
            {text: "Add Appointment",func:"addSome"},
            {text: "Add Test",func:"addSome"},
            {text: "Add Testresult",func:"addSome"}
        ]
    ];  
    if(!Auth.user) {
        $scope.items = loggedOut;   
    } else {
        $scope.items = loggedIN;
    }
    $scope.login = function(){
        openLoginModal();
    };
    // becouse the other function is not in scope.
    $scope.openSettings = function(){
        openSettingsModal();
    };
    $scope.logout = function(){
        Logger.Info(Auth.user.username + " is logging out");
        Auth.logout();
        Auth.user = undefined;
        $scope.items = loggedOut;

    };
    //Minimize app to tray by hiding the window not needed at the moment
    $scope.toTray = function() {
            Window.hide();
    };
    //Minimize app
    $scope.minimize = function() {
            Window.minimize();
    };
    //Close App
    $scope.close = function() {
            Logger.Info("Quiting App");
            require('nw.gui').App.closeAllWindows();
    };

    // other button functions.
    $scope.addSome = function(type){
        type = type.split(/\s+/)[1].toLowerCase(); 
        openAddModal(type);
    };
    function addToDB(type,item){
        if(type === 'user'){
            Auth.addUser(item.username,item.password);
            Logger.Info("adding new user " + Auth.user.username);
            // this is very fishey
            DAL.users.insert(Auth.user,function(err,docs){
                if (err) {  console.log('An error occured');}
                //Logger.Debug("added new user " + Auth.user.username + " to DB");
                Auth.usersDB.push(Auth.user);
            });
        }else{
            Logger.Info('Add item:' + type);
            item.type = type;
            item.user = Auth.user.username;
            addItem(item);
        }
        
    }
    $scope.exportData = function(str){
        // this function will export all user data or partial user data.
        // starting by choosing a file
        console.log("Pressed export");
        fileDialog.saveAs(function(filename) {
        var exporter = require('homephr-exporter');
        DAL.get({"user":Auth.user.username},function(err,docs){
            var newDocs = _.map(docs,function(value){return _.omit(value,'_id');});
            exporter.export(newDocs,filename,'json');
        });
        
        
      },'export.json',[".json",".sfr"]);
    };
    $scope.importData = function(str){
        // this function will import all data in selected file.
        // starting by choosing a file
        console.log("Pressed import");
        fileDialog.openFile(function(filename) {
            var exporter = require('homephr-exporter');
            var content = exporter.import(filename);
            _.each(content, function(value){
                addItem(value);
            });
      },false,[".json",".sfr"]);
    };
    function openLoginModal() {
        Logger.Debug('opening login modal');
        var modalInstance = $modal.open({
            templateUrl: 'templates/loginTemplate.html',
            controller: 'LoginModal',
        });
        modalInstance.result.then(function (data) {
                $scope.items = loggedIN;
            }, function () {
                Logger.Debug('Canceled');
            }
        );
    }
    function openSettingsModal() {
        Logger.Debug('opening settings modal');
        var modalInstance = $modal.open({
            templateUrl: 'templates/settingsTemplate.html',
            controller: 'SettingsCtrl',
        });
        modalInstance.result.then(function (data) {
                Logger.Debug('Saved');
            }, function () {
                Logger.Debug('Canceled');
            }
        );
    }
    function openAddModal(type) {
        Logger.Debug('opening modal for type ' + type);
        var modalInstance = $modal.open({
            templateUrl: 'templates/modalTemplate.html',
            controller: 'ModalAddCtrl',
            resolve: {
                type: function() {  return type;    },
                item: function() {  return {};  }
            }
        });
        modalInstance.result.then(function (data) {
            $scope.newItem = data;
            Logger.Debug('returned data: ' + $scope.newItem.type);
            var item = $scope.newItem.item;
            /*
            angular.forEach(item, function(value, key){
                this.Debug('returned data: '+ key + "=" + value);   
            },Logger);
            */
            addToDB(type,item);
                
                
            
            }, function () {
            Logger.Debug('Canceled');
        });
    }
    function addItem(item) {
        DAL.insert(item,true,function(err,doc){
                Logger.Debug('Added Successfully');
                DAL.getIndex(function(items){
                    SmartSearch.setIndex(items);
                });
            });
    }
}]);


