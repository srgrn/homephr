/*jshint sub:true*/
homephr.controller('AlertCtrl', ['$scope','$interval','$http' ,'Window','Logger','DAL','SmartSearch','Auth','Notifications',function($scope,$interval,$http,Window,Logger,DAL,SmartSearch,Auth,Notifications){
		"use strict";
		var _ = require('underscore');
		var run = $interval(getAlertsFromDB, 3000);
		$scope.alerts = [];
		$scope.closeAlert = function(index) {
			var icon = 'assets/vendor/Notifications/desktop-notify.png';
			var title = "HomePHR";
			var content = $scope.alerts[index].msg;
			$scope.alerts[index].canceled = "yes";
			Logger.Info("canceling alert " + $scope.alerts[index]);
			/*window.LOCAL_NW.desktopNotifications.notify(icon, title, content, function(){
				$('#status').fadeIn('fast',function(){
					setTimeout(function(){$('#status').fadeOut('fast');},1800);
				});
			});*/
		};
		$scope.pushItem = function(item){
			//$http.defaults.headers.common['Authorization'] = "Basic djF5RmgxaFJHdGFERDJsaWZMb0x4N3JnMHZmcnpBNHlnRnVqeHpNUVROR1A2Og==";
			$http.defaults.headers.common['Authorization'] = "Basic " + window.btoa(window.settings.pushbulletkey + ":''");
			console.log("Basic " + window.btoa(window.settings.pushbulletkey + ":''"));
			$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
			var pushUrl = "https://api.pushbullet.com/v2/pushes";
			console.log("trying to push item:" + item);
			if(!item.pushed || _.isUndefined(item.pushed)) {
				console.log("trying to push");
				var uricontent = "type=note&title="+ encodeURIComponent("Alert from HomePHR") + "&body="+ encodeURIComponent(item.msg);
				console.log(uricontent);
				$http.post(pushUrl, uricontent).success(function() { 
					item.pushed = true;
					Logger.Debug("Sent message");
					console.log(item.msg + " sent");
					
				}).error(function(data,status){
					console.log(data);
				});
	
			}
			
		};
		function getAlertsFromDB() {
			Notifications.data = [];
			var ret = [];
			if (_.isUndefined(Auth.user)){
				$scope.alerts = [];
				return;
			}
			DAL.get({},function(err,items){

					angular.forEach(items,function(item){
						if(! _.isUndefined(Auth.user) && ! _.isUndefined(item.user) && Auth.user.username === item.user) {
							if(! _.find(Notifications.data,function(element){
								//console.log('item',item,'element',element);
								if(item['_id'].id === element['_id'].id){
									return true;
								}
								return false;
								})){
								//console.log(item);
								Notifications.data.push(item);
							}
						}
					});
					ret = Notifications.getAll(window.settings);
					//console.log(ret);
					angular.forEach(ret,function(item){ 
						//console.log(item);
						if(!_.find($scope.alerts,function(element){
							//console.log(element);
							if(item.msg === element.msg) {
								return true;
							}
							return false;
						})){
							Logger.Info("Adding alert" + item);
							//console.log(item);
							$scope.alerts.push(item);
							$scope.pushItem(item);
						}
						
						//if(_.pluck($scope.alerts,'message'))
					});

				console.log("****",$scope.alerts);
				});
		}
	}
]);