/*jshint sub:true*/
homephr.controller('SearchCtrl', ['$scope','$interval','Window','Logger','DAL','SmartSearch','Auth','$modal',function($scope,$interval,Window,Logger,DAL,SmartSearch,Auth,$modal){
	"use strict";
	var _ = require('underscore');
	var run = $interval(function(){
		$scope.loggedout =  _.isUndefined(Auth.user);
	}, 100);

	$scope.search = function() {
		$scope.results = [];
		var index = SmartSearch.getIndex();
		//console.log(index);
		DAL.getIndex(function(items){
			SmartSearch.setIndex(items);
		});
		var str = $scope.searchText;
		//console.log(str);
		if(str === "") {
			return;
		}
		//DAL.get({'useIndex': str},function(err,items){
		var keys =SmartSearch.search(str,SmartSearch.getIndex(),window.settings.sensetivity,window.settings.caseInSenstive);
		
		//console.log(keys);
		angular.forEach(keys, function(value){
			DAL.get({'useIndex': value.word},function(err,items){
				if (items === null) {return;}
				$scope.$apply(function () { // this is like f***ing magic. it actually forces redarw of the view
					//console.log($scope.results);
					angular.forEach(items,function(item){
						if(!_.isUndefined(Auth.user) && !_.isUndefined(item.user) && Auth.user.username === item.user) {
							if(! _.find($scope.results,function(element){
								//console.log('item',item,'element',element);
								if(item['_id'].id === element['_id'].id){
									return true;
								}
								return false;
								})){
								$scope.results.push(item);
							}
						}
					});
						
					//$scope.results = $scope.results.concat(items);
				});
			});	
		});
		switch (str){
			case 'close':
				Logger.Info("User entered command close");
				Window.close();
				break;
			case 'import':
				Logger.Info("User entered command import");
				break;
			case 'minimize':
				Logger.Info("User entered command minimize");
				Window.minimize();
				break;
		}
		
	};
	function updateDB(item){
		Logger.Debug(item);
		DAL.update(item,function(err,result){
			Logger.Info("Saved update to DB: "+ item);
		});
		
	}
	function openModal(type,item) {
		Logger.Debug('opening modal for type ' + type);
		var modalInstance = $modal.open({
			templateUrl: 'templates/modalTemplate.html',
			controller: 'ModalAddCtrl',
			resolve: {
				type: function() {	return type;	},
				item: function() {	return item;	}
			}
		});
		modalInstance.result.then(function (data) {
			$scope.newItem = data;
			Logger.Debug('returned data: ' + $scope.newItem.type);
			var item = $scope.newItem.item;
			angular.forEach(item, function(value, key){
				this.Debug('returned data: '+ key + "=" + value);	
			},Logger);
				updateDB(item);
			}, function () {
			Logger.Debug('Canceled');
		});
	}

	$scope.edit = function(item){
		Logger.Info("Start update: "+ item);
		openModal(item.type,item);
	};
}]);