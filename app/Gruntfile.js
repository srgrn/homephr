
module.exports = function(grunt) {
	"use strict";
	grunt.initConfig({
	beep: false,
	pkg: grunt.file.readJSON('package.json'),
	concat : {
		options: {
		stripBanners: true,
		banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
			'<%= grunt.template.today("yyyy-mm-dd") %> */',
			},
		dist: {
			src: ['app.js','./directives/*.js','./services/*.js','./controllers/*.js'],
			dest: './<%= pkg.name %>_main.js'
		},
	},
	jshint: {
		app: ['Gruntfile.js','app.js','./directives/*.js', './services/*.js', './controllers/*.js', 'test/**/*.js'],
		modules: ['modules/*/*.js','modules/*/test/*.js'],
		options: {
		// options here to override JSHint defaults
			globals: {
				jQuery: true,
				console: true,
				module: true,
				document: true
			},
			node: true
		}
	},
	watch: {
		files: ['<%= jshint.app %>','<%= jshint.modules %>'],
		tasks: ['jshint:app','jshint:modules','concat']
	},
	nodewebkit: {
		options: {
				version: '0.9.2',
				build_dir: './webkitbuilds', // Where the build version of my node-webkit app is saved
				timestamped_builds: false,
				keep_nw : true,
				win: true,
				mac: false, 
				linux32: false,
				linux64: false
			},
				src: ['build/**'] // Your node-wekit app
			},
	copy: {
		main: {
			files: [
			{expand: true, src: ['HomePHR_main.js','server.js','package.json','index.html'], dest: 'build/', filter: 'isFile'},
			{expand: true, src: ['assets/**','templates/**'], dest: 'build/'}
				]
			}
		},
	clean: {
		build: ["./build", "./webkitbuilds/releases/**","./webkitbuilds/releases"],
		}
	});
	grunt.option('beep',false);
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-node-webkit-builder');
	
	grunt.registerTask('install-dependancies',"run npm install on the build directory",function(){

		var exec = require('child_process').exec;
		var cb = this.async();
		exec('npm install --production', {cwd: './build'}, function(err, stdout, stderr) {
			console.log(stdout);
			cb();
		});
	});
	grunt.registerTask('CreateMSI',"create an installer pacakge",function(){

		var exec = require('child_process').exec;
		var cb = this.async();
		exec('"c:\\Program Files (x86)\\WiX Toolset v3.8\\bin\\candle.exe" ..\\..\\..\\installer.wxs -out installer.wixobj -ext WixUIExtension', {cwd: './webkitbuilds/releases/HomePHR'}, function(err, stdout, stderr) {
			console.log(stdout);
			exec('"c:\\Program Files (x86)\\WiX Toolset v3.8\\bin\\light.exe" installer.wixobj -out HomePHRInstaller.msi -ext WixUIExtension', {cwd: './webkitbuilds/releases/HomePHR'}, function(err, stdout, stderr) {
			console.log(stdout);
			cb();
		});
		});
	});

	grunt.registerTask('default', ['jshint:app','concat']);
	grunt.registerTask('build', ['default','clean','copy','install-dependancies','nodewebkit','CreateMSI']);
	

};