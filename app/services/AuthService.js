homephr.factory('Auth', function(){
	var Auth = require('homephr-auth');
	var path = require('path'); 
	var db = require('homephr-dal')({'path': path.dirname( process.execPath )});
	db.users.find().toArray(function(err,docs){
		Auth.usersDB = docs;
	});
	return Auth;
});