homephr.factory('Logger', function() {
    /*
    var logdate = new Date();
    var dateTimeString = logdate.getFullYear() + "-" + (logdate.getMonth()+1) + "-" + logdate.getDate();
    dateTimeString += "_" + logdate.getHours() + "-"+ logdate.getMinutes() + "-" + logdate.getSeconds();
    var file = "./logs/"+dateTimeString+".log";
    */
    var path = require('path'); 
    var fs = require('fs');
    var logdir = path.dirname( process.execPath ) + "/logs";
    try {
        fs.mkdirSync(logdir);
    }catch (e) {
        console.log("Directory already exist",logdir);
    }
    var file = logdir + "/homePHR.log";
    var logger = require('homephr-logger')(file);
    return logger;
  });
