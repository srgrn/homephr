/*
 this is an evil service for scraping data from drugs.com
*/
/*jshint -W069 */
homephr.factory('Scraper', ['$q' ,function($q){
	var Scraper = {};
	var request = require('request');
	Scraper.getPromiseResults = function(params) {
		var defer = $q.defer();
		var request = require('request');
		request('http://www.drugs.com/js/search.php?id=livesearch-interaction&s=' + params, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				var reg = /interactionsAddDrug\(this,\s?'(\d+)',\s?'(\d+)',\s?'\w*',\s?'(.*)',\s?'(.*)'\)/g;
				var retNames = [];//body.match(reg);
				while ((myArray = reg.exec(body)) !== null)
				{
					
					var obj = {	'__id1':myArray[1], 
								'__id2':myArray[2],
								'name':myArray[3],
								'substance_name':myArray[4], 
					};
					obj['__name_convert'] = obj.name.replace(/\W+/g,'-').toLowerCase();
					obj['__side_effects'] = 'http://www.drugs.com/sfx/' +  obj.__name_convert + '-side-effects.html';
					obj['__interactions_link'] = 'http://www.drugs.com/drug-interactions/' + obj.substance_name.replace(/\//g,'-').replace(/\s/g,'-') + ',' + obj.__name_convert + '-index.html';
					retNames.push(obj);
					/*retNames.push(myArray[3]);
					retNames.push(myArray[4]);*/
				}
				defer.resolve(retNames);
			}
		});
		return defer.promise;
	};
	Scraper.getLink = function(drug){
		console.log(drug);
		var link = 'http://www.drugs.com/' + drug.__name_convert + '.html';
		var defer = $q.defer();
		request(link, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				console.log(link);
				defer.resolve(link);
			} else {
				link = link.replace(/-/g,'_');
				request(link, function (error, response, body) {
					if (!error && response.statusCode == 200) {
						console.log(link);
						defer.resolve(link);
					} else {
						defer.reject();
					}
				});
			}
		});
		return defer.promise;
	};
	Scraper.getInteractionsList = function(drug){
		var defer = $q.defer();
		request(drug.__interactions_link, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				var interaction_reg = /<li class="int_(\d)">.*<a href="(\/drug-interactions\/.*)">(.*)<\/a>/g;
				var line = [];
				var interactions = [];
				while ((line = interaction_reg.exec(body)) !== null)
				{
					//interactions.push({'level':line[1],'link':line[2],'name':line[3]});
					interactions.push({'level':line[1],'name':line[3]});
				}
				defer.resolve(interactions);
			}
		});
	return defer.promise;
	};


	return Scraper;
}]);