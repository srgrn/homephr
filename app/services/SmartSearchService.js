homephr.factory('SmartSearch', function(){
	var SmartSearch = require('homephr-smartsearch');
	var index = {};
	function SetIndex(newIndex) {
		index = newIndex;
	}
	function GetIndex() {
		return index;
	}
	return {
		'search': SmartSearch.search,
		'getIndex': GetIndex,
		'setIndex': SetIndex
	};
});