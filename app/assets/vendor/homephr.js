// this is a starting point

var gui;

function setup(context){
	
	gui = context;
	setMenu();
	win = gui.Window.get();
	win.title = "This is the setup";
	
};

function setMenu() {
	var menubar = new gui.Menu({ type: 'menubar' });
	var HelpMenu = new gui.Menu();
	var FileMenu = new gui.Menu();
	var PlaceHolder = new gui.Menu();

	HelpItems = [ 
		{label: 'Documentation'},
		{label: 'Some other stuff'},
		{type: 'separator' },
		{label: 'About HomePHR', click: openAboutWindow}
	];
	FileItems = [
		{ label: 'Login'},
		{ label: 'New Object'},
		{ type: 'separator' },
		{ label: 'Exit', click : promptForClose}
	];
	poplateMenu(HelpMenu,HelpItems);
	poplateMenu(FileMenu,FileItems);

	menubar.append(new gui.MenuItem({ label: 'File', submenu: FileMenu}));
	menubar.append(new gui.MenuItem({ label: 'Help', submenu: HelpMenu}));
	//poplatePlaceHolder();
	var win = gui.Window.get();
	win.menu = menubar;
};
function poplateMenu(menu,items){
	
	for (var i = 0; i < items.length; i++) {
	 	console.log(items[i]);
		menu.append(new gui.MenuItem(items[i]));
	};

};
function openAboutWindow() {
  var new_win = gui.Window.open('about.html', {
	  position: 'center',
	  width: 250,
	  height: 250,
	  "toolbar": false,
	  "resizable": false,
	  "focus":true
	});
  new_win.on('blur', function() {
  	new_win.close();
	});
}
function promptForClose(){
	var result = window.confirm("Are you sure you want to exit?");
	if(result == true) {
		// here be other close up actions and calls.
		win.close(); 
	}else{
		win.focus();
	}
};
module.exports = setup;