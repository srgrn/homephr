Final Project notes:
====================

Use Cases:
-----------
1. A family with two adults and two children where the father and mother need to keep track of all medical information for the entire family
2. A small private clinic where the owner wishes to keep his patients data in a small private computer?. 
3. A person with specific illness that wants to keep better track of his own appointments and test results to make better informed decisions.

Terms:
------
* "Medical information": this will refer to the types of data objects that can be kept like:
	test-results,
	appointment dates,
	specific doctors contact information.  
* "System": The main software system being developed.  
* "User": the person using the system.
* "HMO": An organization supplying medical care for its members/customers. this is an acronym for Health Maintenance Organization. in israel there are several such organzation For example Maccabe and Clalit.
