## Statement of Work ##


## Introduction & Background ##
In today world where computer use is ubiquitous one of the fields that left behind is the field of medicine, and while many improvements where made in the last couple of years most of the data is hidden away in physical files and folders across hospitals clinics and doctor offices.  
For example when a mother takes her small child to a checkup at the doctor, the doctor usually (even today at clinics that work with multiple health insurers) will fetch the file from his records (a physical folder with the patient history, past tests and other known details list allegories) and will print the results of the check in duplicate once for the file and once for the mother.  
the situation is a bit improved for lab tests where it is possible today to get the results of a blood test sent to you by email.  
however even with the improvements that there are in Israel and many other places the data is split across providers, so if you go to a doctor working with your HMO you will get a reminder from the HMO and see the results on their site (connected to their DB and file on you) but if you will go to an external expert you will probably get the results printed out or as a digital copy (the most common way is a CD). Another issue is the availability of the data, remainders and results regarding other people in your immediate family/care for example a mother for three children would need an easy way to access the data for each child without logging into multiple accounts.

The suggested system is a Personal Medical Record Management System, which will at the first stage assist the user by keeping record of his own, his immediate family, medical data (like results from previous tests) and also interfacing with other systems to make it more streamlined (like interfacing with Google Calender for future appointments).

Collecting data as the first step will allow assisting to better understand test results by allowing for using the Internet resources to automatically search for medical terms.

## Current/Alternative systems ##
The field of PHR is growing in the last years as a result of the increase in computing power and mobile/cloud computing. while there are many systems defined as PHR by many sellers they can be separated according to the following categories:
1. Online applications - This category can be further divided and expended into many types however for simplicity it can be defined as an web site/webapp accessible online that is able to recieve information from the user, and usually share this information back to other people and systems (Like EHRs,offline software). a good example is the now closed [Google health](http://www.google.com/intl/en_us/health/about/), and Microsoft [HealthVault](https://www.healthvault.com/il/en).

2. Stand-Alone programs - while being the precursor for the first category it is today mostly another tool in the online PHR toolbox, basicly any software that is installed locally and using local storage to keep the data. this category sitting between the online PHR and the document template has some of the limitation of the document in being limited to a single place and storage, and not easily shareable, also security is usually not part of the design, however it has more advanced features like notifications, ability to calculate and show more details. today this can also refer to some mobile apps that use local storage to keep the data which while being a standalone apps can still use all the connectivity resulting from the base device.

3. Document templates with security mechanisms - a simple document template that is usually a word or a text file containing medical information usually assisted with some kind of encryption to prevent it from being too accessible and carried in any storage device (USBKey, mobile phone, online storage). while i was unable to locate a modern example this was the base for the other two systems  


In a report from the [Markle Group](http://www.markle.org) from 2004 they are describing in this report two models:
1. merged PHR/EHR systems, like the online tools from Maccabee or Clalit. those tools are usually integrated with a specific provider, where the data is kept and is allowing the client (patient/consumer) to see a window into the data kept by the provider, and sometimes adding his own data
2. A stand alone system, that gathers information from the patient and from multiple professional sources, or pull data from multiple institutions across multiple types of services. a good example is the MS Health Vault which is capeable of connecting and adding data from multiple devices, networks and hospitals.

while this covers the field in overview due to the conecpts like medicine 2.0 and health 2.0 
## Targets ##

## Goals ##

## Indicators/Acceptance Criteria(?) ##

## References(?) ## 

## Deliverables ##
The final Deliverable is a working (extendable?) system built of several modules(?).
The system will run on the windows operating system as a standalone application (not a windows metro app).

## Use Cases ##
Patient
* Enter new item
* Search for item
* Update Existing Item
Calender 
* Set Event
* Update Data
Alert
* Start
all use cases are around data types or data outputs
Use Case 1:
User calls (on phone) to HMO and sets an appointment so after getting the time he enters the details of the doctor with the date and time of the appointment into the system, the system updates his calender 
Use Case 2: 
User gets results of blood test as a file, he opens the system and update the test 