installing gitweb on ubuntu ec2 with nginx and ssl from startssl
----------------------------------------------------------------

## Step 1 - install latest git ##
following the instructions in the [Ubuntu Git Maintainers](https://launchpad.net/~git-core/+archive/ppa) you do the following:  
`sudo add-apt-repository ppa:git-core/ppa`  
`sudo 'echo deb http://ppa.launchpad.net/git-core/ppa/ubuntu precise main > /etc/apt/sources.list.d//git-stable-releases-precise.list'  
sudo 'echo deb-src http://ppa.launchpad.net/git-core/ppa/ubuntu precise main >>/etc/apt/sources.list.d//git-stable-releases-precise.list'`  
`sudo apt-get update`  
`sudo apt-get install git`  

## Step 2 - install nginx and friends ##
using some on-line searching i found a page explaining what i needed.  
so again using apt-get  
`sudo apt-get install nginx fcgiwrap` 
than in order to make it simpler later on you also need a way to generate the simple auth file,
i found it in the [nginx faq](http://wiki.nginx.org/Faq#How_do_I_generate_an_.htpasswd_file_without_having_Apache_tools_installed.3F) it is a simple perl file
`#!/usr/bin/perl  
use strict;  

chomp(my $filename=$ARGV[0]);  
chomp(my $username=$ARGV[1]);  
chomp(my $password=$ARGV[2]);  

if (!$filename || !$username || !$password) {  
  print "USAGE: ./crypt.pl filename username password\n\n";  
} else {  
  open my $fh, ">>", $filename or die $!;  
  print $fh $username . ":" . crypt($password, $username) . "\n";  
  close $fh or die $!;  
}  
`  
save it as crypt.pl we will use it later.  

## step 3 - get a ssl certificate ##
I have used [StartSSL](http://startssl.com) but you can use self-signed if you wish.  
so the steps are below  
1. register to startssl at (http://startssl.com).  
2. register your domain in the validation wizard.  
3. create a csr  
`openssl genrsa -des3 -out server.orig.key 2048` create a private key  
`openssl rsa -in server.orig.key -out server.key` remove the passphrase from the key  
`openssl req -new -key server.key -out server.csr` generate the csr  
4. go into the certification wizard on the startssl site and choose a ssl certificate  
5. paste the contets of the csr into the correct place  
6. wait for the email that the certificate has been approved (took around 25 minutes for me)
7. paste the contents of the certificate into a crt file - ssl.crt  
8. get the ca certificates  
`wget http://www.startssl.com/certs/sub.class1.server.ca.pem`  
`wget http://www.startssl.com/certs/ca.pem`  
`cat ssl.crt sub.class1.server.ca.pem ca.pem > server.crt` - combine all certificates into one file  
9. copy the certificate and the key to the correct places (sudo might be required)
`sudo cp -rf server.crt /etc/ssl/certs/server.crt`  
`sudo sudo cp server.key /etc/ssl/private`  

## step 4 set up the site ## 
1. first is to remove the default site from nginx  
`sudo unlink /etc/nginx/sites-enabled/default`  
2. than create a new file in the sites-available directory and link it  
`sudo touch /etc/nginx/sites-available/git; sudo link /etc/nginx/sites-enabled/git /etc/nginx/sites-available/git`   
3. fill into the /etc/nginx/sites-available/git the content from the example file  
update as necessary  
I also added to nginx.conf the line  under http but it is possible that you can add it in the example file  
`client_max_body_size 50m; `  
4. update the gitweb script to point to your repository directory  
`sudo vim /usr/share/gitweb/gitweb.cgi`  
5. create the access-list file using the crypt.pl from before to generate the lines  
`sudo perl crypt.pl /etc/nginx/access_list <username> <password>`  
6. restart nginx  
`sudo nginx -s reload`  

and you should now be able to use git remotely using https://yourserver/repo 
i still had some issues regarding permissions but it already worked at this stage atleast for clone and such.  
also i had to create an empty repo the <name>.git  
and run `git update-server-info` in it so it will work properly  


